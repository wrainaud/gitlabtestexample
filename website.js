import http from "k6/http";
import { check, group, sleep } from "k6";
import { Counter, Rate, Trend } from "k6/metrics";
import { randomIntBetween, randomItem } from "https://jslib.k6.io/k6-utils/1.0.0/index.js";

const loginData = JSON.parse(open("./users.json"));

export let options = {
    stages: [
       { target: 200, duration: "30s"  },
       { target: 200, duration: "1m" },
       { target: 0, duration: "30s" }
    ],
    thresholds: {
        "http_req_duration": ["p(95)<500"],
        "http_req_duration{staticAsset:yes}": ["p(95)<250"],
        "check_failure_rate": ["rate<0.5"]
    },
    ext: {
        loadimpact: {
            projectID: 3630584,
            name: "Modified Insights Test with Cloud Execution",
            distribution: {
                scenarioLabel1: { loadZone: "amazon:ie:dublin", percent: 25 },
                scenarioLabel2: { loadZone: "amazon:us:ashburn", percent: 25 },
                scenarioLabel3: { loadZone: "amazon:au:sydney", percent: 25 },
                scenarioLabel4: { loadZone: "amazon:us:palo alto", percent: 25 }
            }
        }
    },
};

let successfulLogins = new Counter("successful_logins");
let checkFailureRate = new Rate("check_failure_rate");
let timeToFirstByte = new Trend("time_to_first_byte", true);

export default function() {
    group("Front page", function() {
        let res = null;
        if (__ENV.URL_ALERT) {
            res = http.get("https://test.k6.io/?ts=" + Math.round(randomIntBetween(1,2000)));
        } else {
            res = http.get("https://test.k6.io/?ts=" + Math.round(randomIntBetween(1,2000)), { tags: { name: "http://test.k6.io/ Aggregated"}});
        }
        let checkRes = check(res, {
            "Homepage body size is 11026 bytes": (r) => r.body.length === 11026,
            "Homepage welcome header present": (r) => r.body.indexOf("Welcome to the k6.io demo site!") !== -1,
            "status is 200": (r) => r.status === 200
        });

        // Record check failures
        checkFailureRate.add(!checkRes);

        // Record time to first byte and tag it with the URL to be able to filter the results in Insights
        timeToFirstByte.add(res.timings.waiting, { ttfbURL: res.url });

        // Load static assets
        group("Static assets", function() {
            let res = http.batch([
                ["GET", "https://test.k6.io/static/css/site.css", {}, { tags: { staticAsset: "yes", otherTag: "CSS", name: "CSS" } }],
                ["GET", "https://test.k6.io/static/js/prisms.js", {}, { tags: { staticAsset: "yes", otherTag: "JS", name: "JS" } }],
            ]);
            checkRes = check(res[0], {
                "Is stylesheet 4859 bytes?": (r) => r.body.length === 4859,
                "status is 200": (r) => r.status === 200
            });

            // Record check failures
            checkFailureRate.add(!checkRes);

            // Record time to first byte and tag it with the URL to be able to filter the results in Insights
            timeToFirstByte.add(res[0].timings.waiting, { ttfbURL: res[0].url, staticAsset: "yes" });
            timeToFirstByte.add(res[1].timings.waiting, { ttfbURL: res[1].url, staticAsset: "yes" });
        });


    });

    sleep(randomIntBetween(1,5));

    group("Login", function() {

        let res = http.get("https://test.k6.io/my_messages.php");
        let checkRes = check(res, {
            "Users should not be auth'd. Is unauthorized header present?": (r) => r.body.indexOf("Unauthorized") !== -1
        });

        //extracting the CSRF token from the response

        const vars = {};

        vars["csrftoken"] = res
            .html()
            .find("input[name=csrftoken]")
            .first()
            .attr("value");

        // Record check failures
        checkFailureRate.add(!checkRes);

        let position = Math.floor(Math.random()*loginData.users.length);
        let credentials = loginData.users[position];

        res = http.post("https://test.k6.io/login.php", { login: credentials.username, password: credentials.password, redir: '1', csrftoken: `${vars["csrftoken"]}` });
        checkRes = check(res, {
            "is logged in welcome header present": (r) => r.body.indexOf("Welcome, admin!") !== -1,
            "status is 200": (r) => r.status === 200
        });

        // Record successful logins
        if (checkRes) {
            successfulLogins.add(1);
        }

        // Record check failures
        checkFailureRate.add(!checkRes, { page: "login" });

        // Record time to first byte and tag it with the URL to be able to filter the results in Insights
        timeToFirstByte.add(res.timings.waiting, { ttfbURL: res.url });

        sleep(10);
        console.log(__ENV.MY_NOTE);
    });

  }