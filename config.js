let localTestOptions = {
    thresholds: {
        http_req_duration: ['avg > 1000'], // Response times average above 1s
        http_req_failed: ['rate > 0.01'],  // Error rate above 1%
    },
    scenarios: {
        Login: {
            executor: 'constant-vus',
            gracefulStop: '30s',
            duration: '30s',
            vus: 20,
            exec: 'login',
        },
        Purchasing: {
            executor: 'ramping-vus',
            gracefulStop: '30s',
            stages: [
                { target: 20, duration: '5s' },
                { target: 80, duration: '25s' },
                { target: 0, duration: '30s' },
            ],
        startVUs: 10,
        exec: 'purchasing',
        },
    },
};
export { localTestOptions };
    
let cloudTestOptions = {
    thresholds: { 
        http_req_duration: ['avg > 1000'], // Response times average above 1s
        http_req_failed: ['rate > 0.01'],  // Error rate above 1%
    },
    ext: {
    loadimpact: {
        projectID: 3630584,
        name: "Load Test with GitLab CI/CD Integration",
        distribution: {
            scenarioLabel1: { loadZone: "amazon:ie:dublin", percent: 40 },
            scenarioLabel2: { loadZone: "amazon:us:ashburn", percent: 30 },
            scenarioLabel3: { loadZone: "amazon:us:columbus", percent: 30 }
        },
        note: `${__ENV.MY_NOTE}`,
    }
    },
    scenarios: {
        Login: {
            executor: 'constant-vus',
            gracefulStop: '30s',
            duration: '2m',
            vus: 20,
            exec: 'login',
        },
        Purchasing: {
            executor: 'ramping-vus',
            gracefulStop: '30s',
            stages: [
                { target: 20, duration: '30s' },
                { target: 80, duration: '1m' },
                { target: 0, duration: '30s' },
            ],
        startVUs: 10,
        exec: 'purchasing',
        },
    },
};
export { cloudTestOptions };