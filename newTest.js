import { sleep, check, group } from 'k6'
import http from 'k6/http'
import jsonpath from 'https://jslib.k6.io/jsonpath/1.0.2/index.js'

export const options = {
  ext: {
    loadimpact: {
        projectID: 3630584,
        name: "New Demo Test with Gitlab CI and Cloud Execution",
        distribution: {
            scenarioLabel1: { loadZone: "amazon:ie:dublin", percent: 40 },
            scenarioLabel2: { loadZone: "amazon:us:ashburn", percent: 30 },
            scenarioLabel3: { loadZone: "amazon:us:columbus", percent: 30 }
        },
        note: `${__ENV.MY_NOTE}`,
   }
  },
  thresholds: {
    http_req_duration: ['p(95)<50000']
  },
  scenarios: {
    Croc_Flow: {
      executor: 'ramping-vus',
      startVUs: 0,
      stages: [
        { duration: '30s', target: 50 },
        { duration: '1m', target: 100 },
        { duration: '30s', target: 0 },
      ],
      exec: 'croc_Flow',
    },
  },
}

export function croc_Flow() {
  let response

  const vars = {}

  // Get Public Crocs
  response = http.get('https://test-api.k6.io/public/crocodiles/')
  check(response, {
    '$[0].name equals Bert': response =>
      jsonpath.query(response.json(), '$[0].name').some(value => value === 'Bert'),
    'status equals 200': response => response.status.toString() === '200',
  })

  group('Login - Login group for Users', function () {
    // Auth
    response = http.post(
      'https://test-api.k6.io/auth/token/login/',
      '{\n   "username":"mark",\n   "password":"securepassword1"\n}',
      {
        headers: {
          'content-type': 'application/json',
        },
      }
    )

    vars['token'] = jsonpath.query(response.json(), '$.access')[0]

    // Private Crocs
    response = http.get('https://test-api.k6.io/my/crocodiles/', {
      headers: {
        Authorization: `Bearer ${vars['token']}`,
      },
    })
    check(response, {
      '$[0].name equals Jerry': response =>
        jsonpath.query(response.json(), '$[0].name').some(value => value === 'Jerry'),
      '$[1].name equals Steve': response =>
        jsonpath.query(response.json(), '$[1].name').some(value => value === 'Steve'),
      '$[1].name equals Bert': response =>
        jsonpath.query(response.json(), '$[1].name').some(value => value === 'Bert'),
    })
    sleep(2)
  })
}