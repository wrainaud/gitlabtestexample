import http from 'k6/http'
import { sleep, check, group } from 'k6'
import { localTestOptions, cloudTestOptions} from "./config.js";

if (__ENV.MY_NOTE) {'local' === localTestOptions} else {'cloud' === cloudTestOptions};

export const options = __ENV.MY_NOTE ? localTestOptions : cloudTestOptions;

// Scenario: Login (executor: constant-vus)

export function login() {
  let response

  const vars = {}

  group('eCommerce - http://ecommerce.test.k6.io/', function () {
    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=add_to_cart',
      {
        product_sku: 'woo-beanie',
        product_id: '16',
        quantity: '1',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.4)
  })

  group('Cart - http://ecommerce.test.k6.io/cart/', function () {
    response = http.get('http://ecommerce.test.k6.io/cart/', {
      headers: {
        'upgrade-insecure-requests': '1',
      },
    })
    check(response, { 'status equals 200': response => response.status.toString() === '200' })

    vars['woocommerce-cart-nonce1'] = response
      .html()
      .find('input[name=woocommerce-cart-nonce]')
      .first()
      .attr('value')

    vars['_wp_http_referer1'] = response
      .html()
      .find('input[name=_wp_http_referer]')
      .first()
      .attr('value')

    sleep(3.8)

    response = http.post(
      'http://ecommerce.test.k6.io/cart/',
      {
        'cart[c74d97b01eae257e44aa9d5bade97baf][qty]': '3',
        coupon_code: '',
        'woocommerce-cart-nonce': `${vars['woocommerce-cart-nonce1']}`,
        _wp_http_referer: `${vars['_wp_http_referer1']}`,
        update_cart: 'Update Cart',
      },
      {
        headers: {
          accept: 'text/html, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.3)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
      {
        time: '1680195528254',
      },
      {
        headers: {
          accept: '*/*',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.6)
  })

  group('Checkout - http://ecommerce.test.k6.io/checkout/', function () {
    response = http.get('http://ecommerce.test.k6.io/checkout/', {
      headers: {
        'upgrade-insecure-requests': '1',
      },
    })
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        security: '599eaaa659',
        payment_method: 'cod',
        country: 'US',
        state: 'CO',
        postcode: '88888',
        city: 'Sussex',
        address: '101 main street',
        address_2: '',
        s_country: 'US',
        s_state: 'CO',
        s_postcode: '88888',
        s_city: 'Sussex',
        s_address: '101 main street',
        s_address_2: '',
        has_full_address: 'true',
        post_data:
          'billing_first_name=Bill&billing_last_name=Rainaud&billing_company=&billing_country=US&billing_address_1=101%20main%20street&billing_address_2=&billing_city=Sussex&billing_state=CO&billing_postcode=88888&billing_phone=888-888-8888&billing_email=bill%40k6.org&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=f22259aeee&_wp_http_referer=%2Fcheckout%2F',
      },
      {
        headers: {
          accept: '*/*',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(6.7)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=checkout',
      {
        billing_first_name: 'Bill',
        billing_last_name: 'Rainaud',
        billing_company: '',
        billing_country: 'US',
        billing_address_1: '101 main street',
        billing_address_2: '',
        billing_city: 'Sussex',
        billing_state: 'CO',
        billing_postcode: '88888',
        billing_phone: '888-888-8888',
        billing_email: 'bill@k6.org',
        order_comments: '',
        payment_method: 'cod',
        'woocommerce-process-checkout-nonce': 'f22259aeee',
        _wp_http_referer: '/?wc-ajax=update_order_review',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, {
      'status does not contain 500': response => !response.status.toString().includes('500'),
    })
    sleep(4.3)
  })

  group('Order Confirmation - http://ecommerce.test.k6.io/checkout/order-received/28942/?key=wc_order_3bJ6kCiQPEr81', function () {
      response = http.get(
        'http://ecommerce.test.k6.io/checkout/order-received/28942/?key=wc_order_3bJ6kCiQPEr81',
        {
          headers: {
            'upgrade-insecure-requests': '1',
          },
        }
      )
      check(response, { 'status equals 200': response => response.status.toString() === '200' })
      sleep(1.3)

      response = http.post(
        'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
        {
          time: '1680195547395',
        },
        {
          headers: {
            accept: '*/*',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'x-requested-with': 'XMLHttpRequest',
          },
        }
      )
      check(response, {
        'status does not contain 500': response => !response.status.toString().includes('500'),
      })
    }
  )
}

// Scenario: Purchasing (executor: ramping-vus)

export function purchasing() {
  let response

  const vars = {}

  group('eCommerce - http://ecommerce.test.k6.io/', function () {
    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=add_to_cart',
      {
        product_sku: 'woo-beanie',
        product_id: '16',
        quantity: '1',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.4)
  })

  group('Cart - http://ecommerce.test.k6.io/cart/', function () {
    response = http.get('http://ecommerce.test.k6.io/cart/', {
      headers: {
        'upgrade-insecure-requests': '1',
      },
    })
    check(response, { 'status equals 200': response => response.status.toString() === '200' })

    vars['woocommerce-cart-nonce1'] = response
      .html()
      .find('input[name=woocommerce-cart-nonce]')
      .first()
      .attr('value')

    vars['_wp_http_referer1'] = response
      .html()
      .find('input[name=_wp_http_referer]')
      .first()
      .attr('value')

    sleep(3.8)

    response = http.post(
      'http://ecommerce.test.k6.io/cart/',
      {
        'cart[c74d97b01eae257e44aa9d5bade97baf][qty]': '3',
        coupon_code: '',
        'woocommerce-cart-nonce': `${vars['woocommerce-cart-nonce1']}`,
        _wp_http_referer: `${vars['_wp_http_referer1']}`,
        update_cart: 'Update Cart',
      },
      {
        headers: {
          accept: 'text/html, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.3)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
      {
        time: '1680195528254',
      },
      {
        headers: {
          accept: '*/*',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.6)
  })

  group('Checkout - http://ecommerce.test.k6.io/checkout/', function () {
    response = http.get('http://ecommerce.test.k6.io/checkout/', {
      headers: {
        'upgrade-insecure-requests': '1',
      },
    })
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        security: '599eaaa659',
        payment_method: 'cod',
        country: 'US',
        state: 'CO',
        postcode: '88888',
        city: 'Sussex',
        address: '101 main street',
        address_2: '',
        s_country: 'US',
        s_state: 'CO',
        s_postcode: '88888',
        s_city: 'Sussex',
        s_address: '101 main street',
        s_address_2: '',
        has_full_address: 'true',
        post_data:
          'billing_first_name=Bill&billing_last_name=Rainaud&billing_company=&billing_country=US&billing_address_1=101%20main%20street&billing_address_2=&billing_city=Sussex&billing_state=CO&billing_postcode=88888&billing_phone=888-888-8888&billing_email=bill%40k6.org&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=f22259aeee&_wp_http_referer=%2Fcheckout%2F',
      },
      {
        headers: {
          accept: '*/*',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(6.7)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=checkout',
      {
        billing_first_name: 'Bill',
        billing_last_name: 'Rainaud',
        billing_company: '',
        billing_country: 'US',
        billing_address_1: '101 main street',
        billing_address_2: '',
        billing_city: 'Sussex',
        billing_state: 'CO',
        billing_postcode: '88888',
        billing_phone: '888-888-8888',
        billing_email: 'bill@k6.org',
        order_comments: '',
        payment_method: 'cod',
        'woocommerce-process-checkout-nonce': 'f22259aeee',
        _wp_http_referer: '/?wc-ajax=update_order_review',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'x-requested-with': 'XMLHttpRequest',
        },
      }
    )
    check(response, {
      'status does not contain 500': response => !response.status.toString().includes('500'),
    })
    sleep(4.3)
  })

  group('Order Confirmation - http://ecommerce.test.k6.io/checkout/order-received/28942/?key=wc_order_3bJ6kCiQPEr81', function () {
      response = http.get(
        'http://ecommerce.test.k6.io/checkout/order-received/28942/?key=wc_order_3bJ6kCiQPEr81',
        {
          headers: {
            'upgrade-insecure-requests': '1',
          },
        }
      )
      check(response, { 'status equals 200': response => response.status.toString() === '200' })
      sleep(1.3)

      response = http.post(
        'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
        {
          time: '1680195547395',
        },
        {
          headers: {
            accept: '*/*',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'x-requested-with': 'XMLHttpRequest',
          },
        }
      )
      check(response, {
        'status does not contain 500': response => !response.status.toString().includes('500'),
      })
    }
  )
}